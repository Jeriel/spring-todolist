package com.ciandt.todolist.job;

import com.ciandt.todolist.model.Todo;
import com.ciandt.todolist.repository.TodoRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Random;


@Component
public class TodoJob {
   private static final Logger LOGGER = LoggerFactory.getLogger(TodoJob.class);

   private final TodoRepository todoRepository;

   @Autowired
   public TodoJob(TodoRepository todoRepository)  {
      this.todoRepository = todoRepository;
   }

   @Scheduled(cron = "0/10 * * ? * *")
   public  void execute() {
      LOGGER.info("Iniciando o job TodoJob");
      try {
         readFile();
      } catch (Exception e) {
         LOGGER.error("Ocorreu um erro no job TodoJob", e);
      }
   }

   private void readFile() {

     LOGGER.info("Lendo csv");

     try {

        CSVReader reader = new CSVReaderBuilder(new FileReader("meucsv.csv"))
                .withSkipLines(1)
                .build();

        String[] nextLine;
        while((nextLine = reader.readNext()) != null) {
           Todo todo = new Todo();
           todo.setTitle(nextLine[0]);
           todo.setDescription(nextLine[1]);
           Todo dbTodo = todoRepository.save(todo);
          LOGGER.info("Inserindo o todo: ", dbTodo);
        }
     } catch (IOException ex) {
        LOGGER.error("Erro ao ler o csv", ex);
     }
   }
}
