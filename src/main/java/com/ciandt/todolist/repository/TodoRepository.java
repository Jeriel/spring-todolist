package com.ciandt.todolist.repository;

import com.ciandt.todolist.model.Todo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface TodoRepository extends PagingAndSortingRepository<Todo, Long> {

//    @Query("SELECT t FROM Todo t WHERE t.title = ?1")
    Page<Todo> findByTitle(String  title, Pageable pageable);

    @Query("SELECT t FROM Todo t WHERE t.title = ?1 AND t.id = ?2")
    List<Todo> findByTitleAndId(String  title, Long id);
}
