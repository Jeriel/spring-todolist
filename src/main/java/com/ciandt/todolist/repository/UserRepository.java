package com.ciandt.todolist.repository;

import com.ciandt.todolist.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository  extends CrudRepository<User, Long> {
    User findByUserName(String userName);
}
